package com.bank.peertopeercurrencyexchange.model;

import java.io.Serializable;

public class ProcessedOrders extends UserOrder implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int processedOrderId;
	private double buy;
	private double rate;
	private double fee;
	public int getProcessedOrderId() {
		return processedOrderId;
	}
	public void setProcessedOrderId(int processedOrderId) {
		this.processedOrderId = processedOrderId;
	}
	public double getBuy() {
		return buy;
	}
	public void setBuy(double buy) {
		this.buy = buy;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ProcessedOrders [processedOrderId=" + processedOrderId + ", buy=" + buy + ", rate=" + rate + ", fee="
				+ fee + "]";
	}
	

}
