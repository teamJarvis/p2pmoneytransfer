package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

public class BaseUtil {
	
	FileInputStream fis;
	protected Properties prop = new Properties();
	WebDriver driver;
	
	
	
	@BeforeSuite
	public void loadProperty()
	{
	File file = new File(System.getProperty("user.dir") +"/datafile.properties");
	try{
    fis = new FileInputStream(file);
	}
	catch(FileNotFoundException e)
	{
		e.printStackTrace();
	}
	
     prop = new Properties();
	try {
		prop.load(fis);
	} catch (IOException e) {
		e.printStackTrace();
	}
  }
	
    @DataProvider(name="getData")
	public Object[][] getData() throws IOException
	   {    
		   XSSFRow row = null;
		   File file1 = new File(System.getProperty("user.dir") +"/src/main/resources/testdata/testdata.xlsx");
		   FileInputStream inputStream = new FileInputStream(file1);
		   Workbook loanWb = new XSSFWorkbook(inputStream);
		   
		   Sheet countrySheet = loanWb.getSheet("country");
		   int rowCount = countrySheet.getLastRowNum()+1;
		   //System.out.println(rowCount);
		   
		   String[][] obj = new String[rowCount][countrySheet.getRow(0).getLastCellNum()];
		   
		   for(int i=0; i<=rowCount-1; i++)
		   {
			   row = (XSSFRow) countrySheet.getRow(i);
			   //System.out.println(row.getLastCellNum());
			   for(int j=0; j<=row.getLastCellNum()-1; j++)
			   {
				   //System.out.println(row.getCell(j).getStringCellValue());
				   obj[i][j] = row.getCell(j).getStringCellValue();
						   
			   }
			   
		   }
		   
		   return obj;
		   
		/*    return new Object[][]
				   {
				   {"harsh", "pwd"},
				   {"monu", "pwd1"}
				   };*/
				   
	   }
	
}
