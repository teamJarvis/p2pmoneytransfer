package com.bank.peertopeercurrencyexchange.service;

import java.util.List;

import com.bank.peertopeercurrencyexchange.model.OrdersCount;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;

public interface ProcessedOrderService {
	public List<ProcessedOrders> getAllOrders();

	public List<OrdersCount> getOrdersCount();
}
