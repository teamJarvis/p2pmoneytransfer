package com.bank.peertopeercurrencyexchange.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.peertopeercurrencyexchange.model.OrdersCount;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;
import com.bank.peertopeercurrencyexchange.model.UserOrder;
import com.bank.peertopeercurrencyexchange.service.ProcessedOrderService;
import com.bank.peertopeercurrencyexchange.service.UserOrderService;

@RestController
@CrossOrigin
public class OrderController {
	
	@Autowired
	UserOrderService userOrderService;
	@Autowired
	ProcessedOrderService processedOrderService;
	
	@RequestMapping(value = "/placeOrder", method = RequestMethod.POST)
	public String placeOrder(@RequestBody UserOrder order) {
		userOrderService.saveOrder(order);
		return "OK";
	}
	
	@RequestMapping(value = "/getOrderPojo", method = RequestMethod.GET)
	public UserOrder getUserPojo(){
		UserOrder order = new UserOrder();
		order.setFrom("EUR");
		order.setTo("GBP");
		order.setToSell(6000);
		order.setTimePlaced(new Date());
		order.setOrginCountry("ES");
		return order;
	}

	@RequestMapping(value = "/getOrders", method = RequestMethod.GET)
	public List<ProcessedOrders> oderList() {

		return processedOrderService.getAllOrders();
	}
	
	@RequestMapping(value = "/getOrdersCount", method = RequestMethod.GET)
	public List<OrdersCount> getOrdersCount(){
		return processedOrderService.getOrdersCount();
	}
	
}
