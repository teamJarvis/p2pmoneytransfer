package com.bank.peertopeercurrencyexchange.model;
import java.io.Serializable;
import java.util.Date;

public class UserOrder implements Serializable {
	
	private int userId;
	private String from;
	private String to;
	private double toSell;
	private Date timePlaced;
	private String orginCountry;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public double getSell() {
		return toSell;
	}
	public void setToSell(double toSell) {
		this.toSell = toSell;
	}
	public Date getTimePlaced() {
		return timePlaced;
	}
	public void setTimePlaced(Date timePlaced) {
		this.timePlaced = timePlaced;
	}
	public String getOrginCountry() {
		return orginCountry;
	}
	public void setOrginCountry(String orginCountry) {
		this.orginCountry = orginCountry;
	}
	@Override
	public String toString() {
		return "UserOrder [userId=" + userId + ", from=" + from + ", to=" + to + ", toSell=" + toSell + ", timePlaced="
				+ timePlaced + ", orginCountry=" + orginCountry + "]";
	}
	
	
	
	

}
