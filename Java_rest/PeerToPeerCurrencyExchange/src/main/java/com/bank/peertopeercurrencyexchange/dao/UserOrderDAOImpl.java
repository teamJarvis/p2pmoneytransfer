package com.bank.peertopeercurrencyexchange.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bank.peertopeercurrencyexchange.model.UserOrder;

@Repository
public class UserOrderDAOImpl implements UserOrderDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	private Environment env;

	public void saveOrder(UserOrder order) {
		String q = "insert into user_order (ORD_USERID, ORD_CCY_FRM, ORD_CCY_TO, ORD_TO_SELL, ORD_CNTRY) values(?, ?, ?, ?, ?)";
		jdbcTemplate.update(q, order.getUserId(), order.getFrom(), order.getTo(),
				order.getSell(), order.getOrginCountry());
	}

	public UserOrder getUserOrderByOrderId(int orderId) {
		UserOrder order = new UserOrder();
//		jdbcTemplate.query(psc, rse)
		return order;
	}

}
