package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.BaseUtil;

public class AdminPage extends BaseUtil {
	
	protected WebDriver driver;
	
	
	@FindBy(how=How.XPATH, using="html/body/p2p-money/div/div/transcations-details/h1")
     WebElement header;
	
	@FindBy(how=How.XPATH, using=".//*[@id='filter']")
	WebElement countrySearch;
	
	
	public String captureHeader(WebDriver driver)
	{
	    this.driver=driver;
		//System.out.println(header.getText());
	    return header.getText();
	
	}
	
	public void searchCountry(WebDriver driver, String country)
	{
		this.driver=driver;
		countrySearch.sendKeys(country);
	}

}
