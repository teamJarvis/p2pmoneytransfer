package com.bank.peertopeercurrencyexchange.model;

import java.io.Serializable;
import java.util.List;

public class ProcessedOrderList implements Serializable{
	private List<ProcessedOrders> pOrderedList;

	@Override
	public String toString() {
		return "ProcessedOrderList [processedOrderList=" + pOrderedList + "]";
	}

	public List<ProcessedOrders> getProcessedOrderList() {
		return pOrderedList;
	}

	public void setProcessedOrderList(List<ProcessedOrders> processedOrderList) {
		this.pOrderedList = processedOrderList;
	}

}
