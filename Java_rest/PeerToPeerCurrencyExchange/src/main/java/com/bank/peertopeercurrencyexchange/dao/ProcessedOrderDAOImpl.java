package com.bank.peertopeercurrencyexchange.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bank.peertopeercurrencyexchange.model.OrdersCount;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;

@Repository
public class ProcessedOrderDAOImpl implements ProcessedOrderDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	Environment evnv;
	
	public void persistProcessedOrder(ProcessedOrders po) {
		String q = "insert into processed_orders (PORD_USERID,PORD_CCY_FRM,PORD_CCY_TO,PORD_SELL,PORD_BUY , PORD_RATE , PORD_FEE , PORD_CNTRY) values(?,?,?,?,?,?,?, ?)";
		jdbcTemplate.update(q, po.getUserId(), po.getFrom(), po.getTo(), po.getSell(),po.getBuy(), po.getRate(), po.getFee(), po.getOrginCountry());
	}

	

	 private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProcessedOrderDAOImpl.class);
	public List<ProcessedOrders> fetchorders() {
		List<ProcessedOrders> orderList = null;

		try {
			String q = "SELECT * FROM processed_orders";
			orderList = jdbcTemplate.query(q, new Object[] {}, new RowMapper<ProcessedOrders>() {
				public ProcessedOrders mapRow(ResultSet rs, int rowNum) throws SQLException {
					ProcessedOrders order = new ProcessedOrders();
					order.setUserId(rs.getInt("PORD_USERID"));
					order.setFrom(rs.getString("PORD_CCY_FRM"));
					order.setTo(rs.getString("PORD_CCY_TO"));
					order.setBuy(rs.getFloat("PORD_BUY"));
					order.setToSell(rs.getFloat("PORD_SELL"));
					order.setRate(rs.getFloat("PORD_RATE"));
					order.setFee(rs.getFloat("PORD_FEE"));
					order.setOrginCountry(rs.getString("PORD_CNTRY"));
					order.setTimePlaced(rs.getDate("PORD_FULLFILL_TSP"));
					return order;
				}
			});
		} catch (EmptyResultDataAccessException erdae) {
			logger.error("empty result data access" + erdae);
		} catch (DataAccessException dae) {
			logger.error("data access exception : " + dae);
		}

		return orderList;
	}



	public List<OrdersCount> getOrdersCount() {
		List<OrdersCount> ordersCounts = null;
		String query = "select PORD_CNTRY, Count(*) as Count FROM processed_orders GROUP BY PORD_CNTRY";
		ordersCounts = jdbcTemplate.query(query, new Object[] {}, new RowMapper<OrdersCount>() {
			public OrdersCount mapRow(ResultSet rs, int rowNum) throws SQLException {
				OrdersCount ordercount = new OrdersCount();
				ordercount.setOriginCountry(rs.getString("PORD_CNTRY"));
				ordercount.setCount(rs.getInt("Count"));
				return ordercount;
			}
		});
		return ordersCounts;
	}

}
