package com.bank.peertopeercurrencyexchange.dao;

import com.bank.peertopeercurrencyexchange.model.UserOrder;

public interface UserOrderDAO {

	public void saveOrder(UserOrder order);
	
	public UserOrder getUserOrderByOrderId(int orderId);
	
}
