package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.AdminPage;
import utility.BaseUtil;

public class TestUseCases extends AdminPage{
	
	   public ExtentReports report;
	   public ExtentTest logger;
	   String apiUrl = null;
	   static WebDriver driver = null;
	   String browser = null;
	   String URL = null;
	   BaseUtil bu = new BaseUtil();
	   
	
	   @BeforeTest
	   public void initialise() {
		   
		   apiUrl = prop.getProperty("apiUrl");
		   browser = prop.getProperty("browser"); 
		   URL = prop.getProperty("URL");
		   System.out.println("The url is "+URL);
		   report = new ExtentReports(System.getProperty("user.dir") +"/test-output/MoneyExchange.html", true);
		   report
        .addSystemInfo("Host Name", "Jarvis")
        .addSystemInfo("Environment", "Test Environment")
        .addSystemInfo("User Name", "Harshvardhan Singh Chauhan");
        report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
        
        if(browser.equals("IE"))
        {
      	  System.setProperty("webdriver.ie.driver", System.getProperty(("user.dir") +"/src/main/resources/drivers/IEDriverServer.exe"));
      	  driver=new InternetExplorerDriver();
      	  //driver.get(URL);
        }
        else if(browser.equals("chrome"))
        {   
      	  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +"/src/main/resources/drivers/chromedriver.exe");
     	      driver=new ChromeDriver();
     	      System.out.println("The browser is chrome");
      	 
        }
        else if(browser.equals("firefox"))
        {
        	System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") +"/src/main/resources/drivers/geckodriver.exe");
		    driver = new FirefoxDriver();
			System.out.println("The browser is firefox");
        }
        else
        {
      	  System.out.println("There is no browser specified");
        }
		   
	   }
	   
	   //@Test
	   public void useCase1a()
	   {   logger = report.startTest("Testing the post API");
		   RestAssured.baseURI  = apiUrl;
		   Response r = RestAssured.given()
				.contentType("application/json")
				.body("")
				.when().get("");
		   
		   System.out.println("The status code is "+r.getStatusCode());
		   Assert.assertEquals(200, r.getStatusCode());
		 
		   Assert.assertTrue(true);
		   logger.log(LogStatus.PASS, "The API test is through");
				   
		   
	   }
	   
	   //@Test
	   public void useCase1b()
	   {   
		   logger = report.startTest("Validating the post API");
		   String moneyExchange = "{\"userId\":\"2\",\"from\":\"EUR\",\"to\":\"GBP\",\"timePlaced\":\"1511258826084\",\"originCountry\":\"IN\",\"toSell\":\"500\"}";  
		   RestAssured.baseURI = "http://10.117.189.129:8080/P2P_Currency_Exchange_Platform/placeOrder";
		   
		   Response r = RestAssured.given()
   				.contentType("application/json")
   				.body(moneyExchange)
   				.when().post("");
		   
		   System.out.println("The status code is "+r.getStatusCode());
		   Assert.assertEquals(200, r.getStatusCode());
		   /*boolean b = r.getStatusCode()==200;
		   Assert.assertTrue(b);*/
			Assert.assertTrue(true);
			//To generate the log when the test case is passed
			logger.log(LogStatus.PASS, "Post API is working fine");
		   
	   }
	   
	   @Test
	   public void useCase2a()
	   {  
		   driver.get(URL);
		   logger = report.startTest("Testing Money Exchange Admin UI");
		   logger.log(LogStatus.INFO, "The application is up and running");
		   driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		   driver.manage().window().maximize();
		   AdminPage adminUser = PageFactory.initElements(driver, AdminPage.class);
		   String header = adminUser.captureHeader(driver);
		   //Assert.assertTrue(driver.getCurrentUrl().toLowerCase().contains("ladies"), "The link has been clicked successfully");
		   Assert.assertTrue(header.toLowerCase().contains("transaction"), "The header has been captured successfully");
		   logger.log(LogStatus.PASS, "The desired link is clicked");
	   }
	   
	   @Test(dataProvider="getData", dataProviderClass=BaseUtil.class)
	   public void useCase2b(String country)
	   {   
		   logger = report.startTest("Testing the search country functionality");
		   AdminPage adminUser = PageFactory.initElements(driver, AdminPage.class);
		   
		   adminUser.searchCountry(driver, country);
	   }
	   
	   @AfterMethod
	   public void tearDown(ITestResult result) throws IOException
	   {
		   System.out.println(result.getName());
		   
		   if(!result.getName().toLowerCase().contains("usecase1"));
		   {
			   if(result.getStatus()==ITestResult.SUCCESS)
			   {
				   String screenshotPath = TestUseCases.getScreenshot(driver, result.getName());
				   logger.log(LogStatus.PASS, logger.addScreenCapture(screenshotPath));
			   }
		   }
		   
		 report.endTest(logger);
	     report.flush();
	   }
	   
	   
	   public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException
	   {
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
	               //after execution, you could see a folder "FailedTestsScreenshots" under src folder
			String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
			return destination;
	   }
	 }
