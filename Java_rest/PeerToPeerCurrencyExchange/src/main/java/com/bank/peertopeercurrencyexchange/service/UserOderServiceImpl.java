package com.bank.peertopeercurrencyexchange.service;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.peertopeercurrencyexchange.dao.UserOrderDAO;
import com.bank.peertopeercurrencyexchange.engine.core.P2PEngine;
import com.bank.peertopeercurrencyexchange.engine.core.exception.P2PCoreException;
import com.bank.peertopeercurrencyexchange.model.UserOrder;

@Service
public class UserOderServiceImpl implements UserOrderService{

	@Autowired
	UserOrderDAO userOrderDAO;
	
	@Autowired
	P2PEngine engine;
	
	 private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserOderServiceImpl.class);

	
	public void saveOrder(UserOrder order) {
		try {
			engine.acceptAndProcessOrder(order);
		} catch (P2PCoreException e) {
			logger.error("p2p core execption : " + e);
		}
	}

}
