package com.bank.peertopeercurrencyexchange.engine.core.util;

import java.util.HashMap;
import java.util.Map;

public class CcyExchangeRateUtil {
	private static Map<String, Float> ccyXchgRates = new HashMap<String, Float>();

	static {
		ccyXchgRates.put("EUR:GBP", 0.8862F);
	}

	public static Float getExchangeRateForCcyPair(String frmCcy, String toCcy) {
		
		return ccyXchgRates.get(frmCcy.toUpperCase()+":"+toCcy.toUpperCase());
	}

}
