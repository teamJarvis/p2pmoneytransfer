package test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.AdminPage;



public class TestApiUseCases extends AdminPage{
	
	
	   public ExtentReports report;
	   public ExtentTest logger;
	   String apiUrl;
	
	   @BeforeTest
	   public void initialise() {
		   	
		   apiUrl=prop.getProperty("apiUrl");
		   report = new ExtentReports(System.getProperty("user.dir") +"/test-output/MoneyExchange.html", true);
		   report
           .addSystemInfo("Host Name", "Jarvis")
           .addSystemInfo("Environment", "Test Environment")
           .addSystemInfo("User Name", "Harshvardhan Singh Chauhan");
           report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		   
	   }
	   
	   @Test
	   public void useCase1()
	   {
		   RestAssured.baseURI  = apiUrl;
		   Response r = RestAssured.given()
   				.contentType("application/json")
   				.body("")
   				.when().get("");
		   
		   System.out.println("The status code is "+r.getStatusCode());
		   Assert.assertEquals(200, r.getStatusCode());
		   logger = report.startTest("Testing the post API");
		   Assert.assertTrue(true);
		   logger.log(LogStatus.PASS, "The API test is through");
				   
		   
	   }
	   
	   @AfterMethod
	   public void tearDown(ITestResult result) throws IOException
	   {
		 report.endTest(logger);
	     report.flush();
	   }
	   
	   
	   public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException
	   {
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
	               //after execution, you could see a folder "FailedTestsScreenshots" under src folder
			String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
			return destination;
	   }
	   

}
