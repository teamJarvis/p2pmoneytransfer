package com.bank.peertopeercurrencyexchange.service;

import com.bank.peertopeercurrencyexchange.model.UserOrder;

public interface UserOrderService {

	public void saveOrder(UserOrder order);
	
}
