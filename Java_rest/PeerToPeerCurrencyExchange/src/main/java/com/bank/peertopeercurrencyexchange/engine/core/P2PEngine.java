package com.bank.peertopeercurrencyexchange.engine.core;

import java.util.List;

import com.bank.peertopeercurrencyexchange.engine.core.exception.P2PCoreException;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;
import com.bank.peertopeercurrencyexchange.model.UserOrder;

public interface P2PEngine {
	public boolean acceptAndProcessOrder(UserOrder order) throws P2PCoreException;

	public List<UserOrder> getAllUserOrders() throws P2PCoreException;

	public List<ProcessedOrders> getAllProcessedOrders() throws P2PCoreException;
}
