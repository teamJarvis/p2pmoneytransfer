package com.bank.peertopeercurrencyexchange.engine.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bank.peertopeercurrencyexchange.constants.P2PContants;
import com.bank.peertopeercurrencyexchange.dao.ProcessedOrderDAO;
import com.bank.peertopeercurrencyexchange.dao.UserOrderDAO;
import com.bank.peertopeercurrencyexchange.engine.core.exception.P2PCoreException;
import com.bank.peertopeercurrencyexchange.engine.core.util.CcyExchangeRateUtil;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;
import com.bank.peertopeercurrencyexchange.model.UserOrder;

@Component
public class P2PEngineImpl implements P2PEngine {
	
	@Autowired
	UserOrderDAO userDAO;
	
	@Autowired
	ProcessedOrderDAO poDAO;
	
	private List<UserOrder> userOrders = new ArrayList<UserOrder>();

	private List<ProcessedOrders> processedOrders = new ArrayList<ProcessedOrders>();


	public boolean acceptAndProcessOrder(UserOrder order) throws P2PCoreException {
		userOrders.add(order);
		userDAO.saveOrder(order);
		processOrder(order);
		return false;
	}

	public List<UserOrder> getAllUserOrders() throws P2PCoreException {
		return userOrders;
	}

	public List<ProcessedOrders> getAllProcessedOrders() throws P2PCoreException {
		return processedOrders;
	}
	
	private ProcessedOrders processOrder(UserOrder order) {

		double buy = order.getSell()
				* CcyExchangeRateUtil.getExchangeRateForCcyPair(order.getFrom(), order.getTo());
		double fee = order.getSell() * P2PContants.FEE_RATE;

		buy -= fee;
		
		ProcessedOrders po = new ProcessedOrders();
		po.setBuy(buy);
		po.setFee(fee);
		po.setUserId(order.getUserId());
		po.setFrom(order.getFrom());
		po.setOrginCountry(order.getOrginCountry());
		po.setRate(CcyExchangeRateUtil.getExchangeRateForCcyPair(order.getFrom(), order.getTo()));
		po.setTo(order.getTo());
		po.setFrom(order.getFrom());
		po.setToSell(order.getSell());
		
		processedOrders.add(po);

		poDAO.persistProcessedOrder(po);
		
		return po;
	}


}

