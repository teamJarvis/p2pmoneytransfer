package com.bank.peertopeercurrencyexchange.dao;

import java.util.List;

import com.bank.peertopeercurrencyexchange.model.OrdersCount;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;

public interface ProcessedOrderDAO {
	public void persistProcessedOrder(ProcessedOrders po);
	public List<ProcessedOrders> fetchorders();
	public List<OrdersCount> getOrdersCount();
}
