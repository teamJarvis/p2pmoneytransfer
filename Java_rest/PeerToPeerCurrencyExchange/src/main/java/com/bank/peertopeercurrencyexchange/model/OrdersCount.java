package com.bank.peertopeercurrencyexchange.model;

public class OrdersCount {
	private String originCountry;
	int count;
	
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return "OrdersCount [originCountry=" + originCountry + ", count=" + count + "]";
	}
	
	
}
