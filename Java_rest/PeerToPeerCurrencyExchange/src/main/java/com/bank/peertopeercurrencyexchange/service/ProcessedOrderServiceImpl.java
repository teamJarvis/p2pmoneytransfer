package com.bank.peertopeercurrencyexchange.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.peertopeercurrencyexchange.dao.ProcessedOrderDAO;
import com.bank.peertopeercurrencyexchange.model.OrdersCount;
import com.bank.peertopeercurrencyexchange.model.ProcessedOrders;

/**
 * @author nawaz
 * this service layer will fetch the orders from repository
 *
 */
@Service
public class ProcessedOrderServiceImpl implements ProcessedOrderService{
	@Autowired
	ProcessedOrderDAO processedDao; 
	
	/* This method will fetch the list of orders
	 * @see com.bank.peertopeercurrencyexchange.service.ProcessedOrderService#getAllorders()
	 */
	public List<ProcessedOrders> getAllOrders() {
		return processedDao.fetchorders();
	}

	public List<OrdersCount> getOrdersCount() {
		return processedDao.getOrdersCount();
	}

}
